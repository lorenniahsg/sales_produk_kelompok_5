-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 16 Bulan Mei 2019 pada 16.31
-- Versi server: 10.1.31-MariaDB
-- Versi PHP: 7.1.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pamkelompok`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `pegawai`
--

CREATE TABLE `pegawai` (
  `id` int(30) NOT NULL,
  `nama_pegawai` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  `gaji` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pegawai`
--

INSERT INTO `pegawai` (`id`, `nama_pegawai`, `status`, `gaji`) VALUES
(1, 'h', 'h', 0),
(2, 'k', 'k', 0),
(3, 'b', 'h', 8),
(4, 'n', 'n', 7);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembayaran`
--

CREATE TABLE `pembayaran` (
  `id` int(30) NOT NULL,
  `nama_pembeli` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  `jumlah` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pembayaran`
--

INSERT INTO `pembayaran` (`id`, `nama_pembeli`, `status`, `jumlah`) VALUES
(1, 'Lorennia', 'Lunas', 150000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `stok`
--

CREATE TABLE `stok` (
  `id` int(100) NOT NULL,
  `nama_kategori` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  `jumlah_stok` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `stok`
--

INSERT INTO `stok` (`id`, `nama_kategori`, `status`, `jumlah_stok`) VALUES
(1, 'Indomie', 'Ready', 30),
(2, 'b', 'b', 7);

-- --------------------------------------------------------

--
-- Struktur dari tabel `upload`
--

CREATE TABLE `upload` (
  `id` int(11) NOT NULL,
  `url` text NOT NULL,
  `name` varchar(500) NOT NULL,
  `harga` int(100) NOT NULL,
  `deskripsi` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `upload`
--

INSERT INTO `upload` (`id`, `url`, `name`, `harga`, `deskripsi`) VALUES
(18, 'produk/public/resource/images/Makanan.jpg', 'Makanan', 20000, 'pesanan'),
(19, 'produk/public/resource/images/Teh Gelas.jpg', 'Teh Gelas', 5000, 'karton'),
(20, 'produk/public/resource/images/Minuman Ringan.jpg', 'Minuman Ringan', 7000, 'Bebas memilih '),
(21, 'produk/public/resource/images/gsgs.jpg', 'gsgs', 4949, 'hshsuxj'),
(22, 'produk/public/resource/images/Cemilan.jpg', 'Cemilan', 10000, 'per bungkus'),
(23, 'produk/public/resource/images/Minuman Ringan.jpg', 'Minuman Ringan', 10000, 'per botol'),
(24, 'produk/public/resource/images/Minuman.jpg', 'Minuman', 20000, 'per botol'),
(25, 'produk/public/resource/images/Minuman.jpg', 'Minuman', 5000, 'per buah'),
(26, 'produk/public/resource/images/minuman baru.jpg', 'minuman baru', 20000, 'sjsbsbsna'),
(27, 'produk/public/resource/images/makanan baru.jpg', 'makanan baru', 30000, 'lalallaa'),
(28, 'produk/public/resource/images/sjsns.jpg', 'sjsns', 120000, 'sjbs ssn'),
(29, 'produk/public/resource/images/sbsbsb.jpg', 'sbsbsb', 1235000, 'zhsbbsns'),
(30, 'produk/public/resource/images/Teh.jpg', 'Teh', 10000, 'per buah'),
(31, 'produk/public/resource/images/Es Teh.jpg', 'Es Teh', 20000, 'per buah'),
(32, 'produk/public/resource/images/indomie kari ayam.jpg', 'indomie kari ayam', 25000, 'indonie yg benutrisi dan lezat'),
(33, 'produk/public/resource/images/Snack Pasta Gigi Pepsident.jpg', 'Snack Pasta Gigi Pepsident', 6000, 'Pepsident ini baik untuk perawatan gigi yg berlubang'),
(34, 'produk/public/resource/images/h.jpg', 'h', 0, 'n'),
(35, 'produk/public/resource/images/b.jpg', 'b', 0, 'b'),
(36, 'produk/public/resource/images/b.jpg', 'b', 0, 'n'),
(37, 'produk/public/resource/images/n.jpg', 'n', 0, 'm'),
(38, 'produk/public/resource/images/n.jpg', 'n', 0, 'n'),
(39, 'produk/public/resource/images/b.jpg', 'b', 0, 'n'),
(40, 'produk/public/resource/images/makanan.jpg', 'makanan', 12000, 'amansantai'),
(41, 'produk/public/resource/images/znznbz.jpg', 'znznbz', 458000, 'bsbshsns'),
(42, 'produk/public/resource/images/indomie.jpg', 'indomie', 2500, 'indomieeeeee'),
(43, 'produk/public/resource/images/Makanan Ringan.jpg', 'Makanan Ringan', 5000, 'harga per item');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `stok`
--
ALTER TABLE `stok`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `upload`
--
ALTER TABLE `upload`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `pembayaran`
--
ALTER TABLE `pembayaran`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `stok`
--
ALTER TABLE `stok`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `upload`
--
ALTER TABLE `upload`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
