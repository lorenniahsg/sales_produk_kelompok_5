<?php
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';

require '../includes/DbOperations.php';

$app = new \Slim\App([
    'settings' => [
        'displayErrorDetails' => true,
    ]
]);

$app->get('/produks', function (Request $request, Response $response) {
    $db = new DbOperations();
    $result = $db->fetch();
    $response->write(json_encode($result['result']));

    return $response
        ->withHeader('Content-type', 'application/json')
        ->withStatus($result['statusCode']);
});

$app->get('/produks/{id}', function (Request $request, Response $response, array $args) {
    $db = new DbOperations();
    $id = $args['id'];
    $result = $db->getById($id);
    $response->write(json_encode($result['result']));

    return $response
        ->withHeader('Content-type', 'application/json')
        ->withStatus($result['statusCode']);
});

$app->run();
