<?php

    class DbOperations {

        private $connection;

        function __construct() {
            require_once dirname(__FILE__) . '/DbConnect.php';

            $db = new DbConnect();
            $this->connection = $db->connect();
        }

        public function fetch() {
            $result = array();
            $produks = array();

            $sql = "SELECT id, url, name, harga, deskripsi FROM upload";
            $stmt = $this->connection->prepare($sql);
            if ($stmt->execute()) {
                $stmt->bind_result($id, $url, $name,  $harga, $deskripsi);
                while($stmt->fetch()) {
                    $produk = array();
                    $produk['id'] = $id;
                    $produk['url'] = $url;
                    $produk['name'] = $name;
                    $produk['harga'] = $harga;
                    $produk['deskripsi'] = $deskripsi;
                    array_push($produks, $produk);
                }
            }

            $result['result'] = $produks;
            $result['statusCode'] = 200;
            return $result;
        }

        public function getById($id) {
            $result = array();

             $sql = "SELECT id, url, name, harga, deskripsi FROM upload WHERE id = ?";
            $stmt = $this->connection->prepare($sql);
            $stmt->bind_param('s', $id);
            $produk = array();
            if ($stmt->execute()) {
                $stmt->bind_result($id, $url, $name, $harga,  $deskripsi);
                if ($stmt->fetch() == null) {
                    $err = array('message' => 'Produk with id ' . $id . ' is not found');
                    $result['result'] = $err;
                    $result['statusCode'] = 404;
                    return $result;
                }

                $produk['id'] = $id;
                $produk['url'] = $url;
                $produk['name'] = $name;
                $produk['harga'] = $harga;            
                $produk['deskripsi'] = $deskripsi;
            }

            $result['result'] = $produk;
            $result['statusCode'] = 200;
            return $result;
        }

    }
